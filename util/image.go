package util

import (
	"fmt"
	"io/ioutil"
	"mime/multipart"
	"net/textproto"
	"os"

	bimg "gopkg.in/h2non/bimg.v1"
)

const (
	img_diractual    = "../public/assets/images/actual"
	img_dirthumbnail = "../public/assets/images/thumbnail"
	img_dirsmall     = "../public/assets/images/small"
)

func UploadPhotoForThumbnail(width int, height int, file multipart.File, handler *multipart.FileHeader) (fileName string, path string, size int64, meme textproto.MIMEHeader, err error) {
	fmt.Printf("Uploading Photo: %+v\n", handler.Filename)
	fmt.Printf("File Size: %+v\n", handler.Size)
	fmt.Printf("MIME Header: %+v\n", handler.Header)

	byteContainer, err := ioutil.ReadAll(file)

	bimg.NewImage(byteContainer).Resize(width, height)
	if err != nil {
		fmt.Fprintln(os.Stderr, err)
	}
	tempFile, err := ioutil.TempFile(os.Getenv("Thumbnail_Imgdir"), fmt.Sprintf("Thumbnail-*%s", handler.Filename))
	uploadPath := tempFile.Name()

	fmt.Println(uploadPath)
	if err != nil {
		fmt.Println(err)
		return "", "", 0, nil, err
	}
	defer tempFile.Close()
	fileBytes, err := ioutil.ReadAll(file)
	if err != nil {
		fmt.Println(err)
		return "", "", 0, nil, err
	}
	tempFile.Write(fileBytes)
	return handler.Filename, uploadPath, handler.Size, handler.Header, err

}
func UploadPhotoForActual(width int, height int, file multipart.File, handler *multipart.FileHeader) (fileName string, path string, size int64, meme textproto.MIMEHeader, err error) {
	byteContainer, err := ioutil.ReadAll(file)

	bimg.NewImage(byteContainer).Resize(width, height)
	if err != nil {
		fmt.Fprintln(os.Stderr, err)
	}
	tempFile, err := ioutil.TempFile(os.Getenv("Actual_Imgdir"), fmt.Sprintf("Actual-*%s", handler.Filename))
	uploadPath := tempFile.Name()

	fmt.Println(uploadPath)
	if err != nil {
		fmt.Println(err)
		return "", "", 0, nil, err
	}
	defer tempFile.Close()
	fileBytes, err := ioutil.ReadAll(file)
	if err != nil {
		fmt.Println(err)
		return "", "", 0, nil, err
	}
	tempFile.Write(fileBytes)
	return handler.Filename, uploadPath, handler.Size, handler.Header, err
}
func UploadPhotoForSmall(width int, height int, file multipart.File, handler *multipart.FileHeader) (fileName string, path string, size int64, meme textproto.MIMEHeader, err error) {
	byteContainer, err := ioutil.ReadAll(file)

	bimg.NewImage(byteContainer).Resize(width, height)
	if err != nil {
		fmt.Fprintln(os.Stderr, err)
	}
	tempFile, err := ioutil.TempFile(os.Getenv("Small_Imgdir"), fmt.Sprintf("Small-*%s", handler.Filename))
	uploadPath := tempFile.Name()

	fmt.Println(uploadPath)
	if err != nil {
		fmt.Println(err)
		return "", "", 0, nil, err
	}
	defer tempFile.Close()
	fileBytes, err := ioutil.ReadAll(file)
	if err != nil {
		fmt.Println(err)
		return "", "", 0, nil, err
	}
	tempFile.Write(fileBytes)
	return handler.Filename, uploadPath, handler.Size, handler.Header, err
}
