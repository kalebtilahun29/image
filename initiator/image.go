package initiator

import (
	routing "image/internal/api/rest"
	"image/internal/handler/rest"
	image "image/internal/module"
	"image/internal/repository"
	"image/internal/storage/persistence"
	"image/platform/db"
	ginRouter "image/platform/gin"
)

// Image initializes the domain image
func Image(dbPlatform db.DbPlatform) []ginRouter.Router {

	// Initiate the image persistence
	imagePersistence := persistence.ImageInit(dbPlatform)
	// Initiate the image repository
	imageRepository := repository.ImageInit()
	// Initiate the image service
	UseCase := image.Initialize(imageRepository, imagePersistence)

	// Initiate the image rest API handler
	handler := rest.ImageInit(UseCase)

	// Initiate the image routing
	return routing.ImageRouting(handler)

}
