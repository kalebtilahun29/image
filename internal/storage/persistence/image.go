package persistence

import (
	"image/internal/constant/model"
	"image/platform/db"
)

// ImagePersistence contains the list of functions for database table image
type ImagePersistence interface {
	AddImage(image *model.Image) (*model.Image, error)
	AddFormaterImage(formater []*model.Formater) ([]*model.Formater, error)
}

type imagePersistence struct {
	db db.DbPlatform
}

// ImageInit is to init the user persistence that contains image data
func ImageInit(db db.DbPlatform) ImagePersistence {
	return &imagePersistence{
		db,
	}
}

func (im *imagePersistence) AddImage(image *model.Image) (*model.Image, error) {
	db, err := im.db.Open()
	if err != nil {
		return nil, err
	}
	dbc, err := db.DB()
	if err != nil {
		return nil, err
	}
	defer dbc.Close()

	err = db.Create(image).Error
	if err != nil {
		return nil, err
	}

	return image, nil
}
func (im *imagePersistence) AddFormaterImage(formater []*model.Formater) ([]*model.Formater, error) {
	db, err := im.db.Open()
	if err != nil {
		return nil, err
	}
	dbc, err := db.DB()
	if err != nil {
		return nil, err
	}
	defer dbc.Close()

	err = db.Create(&formater).Error
	if err != nil {
		return nil, err
	}

	return formater, nil

}
