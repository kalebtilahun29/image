package rest

import (
	"crypto/sha1"
	"fmt"
	"image/internal/constant/model"
	image "image/internal/module"
	"image/util"
	"mime"
	"net/http"
	"strings"

	"github.com/gin-gonic/gin"
)

// ImageHandler contains the function of handler for domain Image
type ImageHandler interface {
	AddImage(c *gin.Context)
}

type imageHandler struct {
	UseCase image.UseCase
}

// ImageInit is to initialize the rest handler for domain Image
func ImageInit(UseCase image.UseCase) ImageHandler {
	return &imageHandler{
		UseCase,
	}
}

func (im *imageHandler) AddImage(c *gin.Context) {
	c.MultipartForm()

	// var image model.Image
	// var formater model.Formater

	imageFile, handler, err := c.Request.FormFile("Image")
	if err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, &model.Response{Error: err.Error()})
		return
	}
	imagename, actualimagePath, size, meme, err := util.UploadPhotoForActual(600, 600, imageFile, handler)
	thumbimagename, thumbnailimagePath, thumbnailsize, _, err := util.UploadPhotoForThumbnail(156, 156, imageFile, handler)
	smallimagename, smallimagePath, smallsize, _, err := util.UploadPhotoForSmall(500, 500, imageFile, handler)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, &model.Response{Error: err.Error()})
		return
	}
	h := sha1.New()
	actualhashname, _ := h.Write([]byte(imagename))
	textToTrim := "image/"
	mimetype, _, err := mime.ParseMediaType(meme.Get("Content-Type"))
	if err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, &model.Response{Error: err.Error()})
		return
	}
	ext := strings.Trim(mimetype, textToTrim)
	image := &model.Image{}
	image.Name = imagename
	image.Alternative_Text = ""
	image.Caption = ""
	image.Width = 600
	image.Height = 600
	image.Hash = "gatsby_" + string(actualhashname)
	image.Ext = ext
	image.Mime = mimetype
	image.Size = size
	image.Url = actualimagePath
	image.Preview_Url = ""
	image.Provider = "local"
	image.Provider_MetaData = ""

	thumbhashname, _ := h.Write([]byte(thumbimagename))

	formater := []*model.Formater{}
	// formater.Name = thumbimagename
	// formater.Width = 156
	// formater.Height = 156
	// formater.Hash = "thumbnail_" + string(thumbhashname)
	// formater.Size = thumbnailsize
	// formater.Url = thumbnailimagePath

	// formater.Name = smallimagename
	// formater.Width = 500
	// formater.Height = 500
	// formater.Hash = "thumbnail_" + string(thumbhashname)
	// formater.Size = smallsize
	// formater.Url = smallimagePath
	formater1 := &model.Formater{
		Name:    thumbimagename,
		ImageID: image.ID,
		Width:   156,
		Height:  156,
		Hash:    "thumbnail_" + string(thumbhashname),
		Size:    thumbnailsize,
		Mime:    image.Mime,
		Path:    "",
		Url:     thumbnailimagePath,
	}
	formater2 := &model.Formater{
		Name:    smallimagename,
		ImageID: image.ID,
		Width:   500,
		Height:  500,
		Hash:    "small_" + string(thumbhashname),
		Size:    smallsize,
		Mime:    image.Mime,
		Path:    "",
		Url:     smallimagePath,
	}
	formater = append(formater, formater1, formater2)
	addimg, err := im.UseCase.AddImage(image)
	im.UseCase.AddFormaterImage(formater)
	if addimg != nil {
		c.JSON(http.StatusInternalServerError, &model.Response{Data: image.Url})
		return
	}
	if err == nil {
		c.JSON(http.StatusOK, &model.Response{Data: image.Url})
		return
	}

	fmt.Println(err)
	c.JSON(http.StatusInternalServerError, &model.Response{Error: err})

}
