package rest

import (
	"net/http"

	"image/internal/handler/rest"
	ginRouter "image/platform/gin"
)

// ImageRouting returns the list of routers for domain image
func ImageRouting(handler rest.ImageHandler) []ginRouter.Router {
	return []ginRouter.Router{
		{ // Upload image
			Method:  http.MethodPost,
			Path:    "/upload/image",
			Handler: handler.AddImage,
			//Middlewares: []gin.HandlerFunc{middleware.ValidateAccessToken(), middleware.ValidatePermission(constant.PermFetchUser)},
		},
	}
}
