package module

import (
	"image/internal/constant/model"
	"image/internal/repository"
	"image/internal/storage/persistence"
)

var imageService *service

// UseCase contains the function of business logic of domain image
type UseCase interface {
	AddImage(image *model.Image) (*model.Image, error)
	AddFormaterImage(formater []*model.Formater) ([]*model.Formater, error)
}

type service struct {
	imageRepo    repository.ImageRepository
	imagePersist persistence.ImagePersistence
}

// Initialize takes all necessary user for domain user to run the business logic of domain user
func Initialize(imageRepo repository.ImageRepository, imagePersist persistence.ImagePersistence) UseCase {
	imageService = &service{
		imageRepo:    imageRepo,
		imagePersist: imagePersist,
	}

	return imageService
}

func ImageService() UseCase {
	return imageService
}
