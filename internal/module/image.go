package module

import "image/internal/constant/model"

func (s *service) AddImage(image *model.Image) (*model.Image, error) {
	return s.imagePersist.AddImage(image)
}
func (s *service) AddFormaterImage(formater []*model.Formater) ([]*model.Formater, error) {
	return s.imagePersist.AddFormaterImage(formater)
}
