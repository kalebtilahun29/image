package model

import (
	"time"

	"gorm.io/gorm"
)

type Image struct {
	ID                uint64     `json:"id" gorm:"primaryKey"`
	Name              string     `json:"name"`
	Alternative_Text  string     `json:"alternative_text"`
	Caption           string     `json:"caption"`
	Width             int64      `json:"width"`
	Height            int64      `json:"height"`
	Hash              string     `json:"string"`
	Ext               string     `json:"ext"`
	Mime              string     `json:"mime"`
	Size              int64      `json:"size"`
	Url               string     `json:"url"`
	Preview_Url       string     `json:"preview_url"`
	Provider          string     `json:"provider"`
	Provider_MetaData string     `json:"provider_metadata"`
	Formater          []Formater `json:"formater"`
	CreatedAt         time.Time
	UpdatedAt         time.Time
	DeletedAt         gorm.DeletedAt `gorm:"index"`
}
type Formater struct {
	ID        uint64 `json:"id" gorm:"primaryKey"`
	Name      string `json:"name"`
	ImageID   uint64 `json:"imageID"`
	Width     int64  `json:"width"`
	Height    int64  `json:"height"`
	Hash      string `json:"string"`
	Ext       string `json:"ext"`
	Mime      string `json:"mime"`
	Size      int64  `json:"size"`
	Path      string `json:"path"`
	Url       string `json:"url"`
	Image     Image  `gorm:"foreignKey:ImageID"`
	CreatedAt time.Time
	UpdatedAt time.Time
	DeletedAt gorm.DeletedAt `gorm:"index"`
}
