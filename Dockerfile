# Build stage
FROM golang:alpine AS builder
WORKDIR /
ADD . .
RUN go build -o images cmd/main.go


# Deployment package
FROM alpine:latest
WORKDIR /
COPY --from=builder images .
RUN mkdir -p public/assets/images/actual && mkdir -p assets/small && mkdir -p assets/thumbnail
RUN chmod +x images

EXPOSE 8091
ENTRYPOINT [ "./images" ]